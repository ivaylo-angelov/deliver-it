package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.repositories.contracts.CityRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CityServiceImplTests {

    @Mock
    CityRepository repository;

    @InjectMocks
    CityServiceImpl service;

    @Test
    public void getAll_Should_Call_Repository() {
        service.getAll();
        verify(repository, times(1)).getAll();
    }

    @Test
    public void getById_Should_Call_Repository_When_CityExist() {
        service.getById(1L);
        verify(repository, times(1)).getById(1L);
    }

    @Test
    public void getById_Should_Throw_When_City_DoesntExist() {
        when(repository.getById(1L))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(1L));
    }

    @Test
    public void getByName_Should_Call_Repository_When_CityExist() {
        service.getByName("name");
        verify(repository, times(1)).getByName("name");
    }

    @Test
    public void getByName_Should_Throw_When_City_DoesntExist() {
        when(repository.getByName("name"))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getByName("name"));
    }

}
