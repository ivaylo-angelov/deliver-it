package com.telerikacademy.web.deliverit.helpers;

import com.telerikacademy.web.deliverit.models.*;

import java.util.Set;

public class EmployeeHelper {

    public static Employee createMockEmployee() {
        var mockEmployee = new Employee();
        mockEmployee.setId(1L);
        mockEmployee.setUser(createMockUser());
        return mockEmployee;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1L);
        mockUser.setEmail("mock@email");
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setRoles(Set.of(createMockRole()));
        return mockUser;
    }

    public static Role createMockRole() {
        var mockRole = new Role();
        mockRole.setName("Employee");
        mockRole.setId(1L);
        return mockRole;
    }
}
