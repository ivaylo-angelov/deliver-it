package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.Employee;
import com.telerikacademy.web.deliverit.repositories.contracts.EmployeeRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@Transactional
public class EmployeeRepositoryImpl implements EmployeeRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public EmployeeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Employee> getAll(Optional<String> username, Optional<String> email,Optional<String> firstName, Optional<String> lastName) {

        try (Session session = sessionFactory.openSession()) {
            Query<Employee> query = session.createQuery("from Employee as e " +
                    "where e.user.username like concat('%', :username,'%') and e.user.email like concat('%', :email,'%') and " +
                    " e.user.firstName like concat('%', :firstName,'%') and e.user.lastName like concat('%', :lastName,'%') ", Employee.class);
            query.setParameter("username", username.orElse(""));
            query.setParameter("email", email.orElse(""));
           query.setParameter("firstName", firstName.orElse(""));
           query.setParameter("lastName", lastName.orElse(""));

            List<Employee> employees = query.list();

            return employees;
        }
    }

    @Override
    public Employee getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            Employee employee = session.get(Employee.class, id);
            if (employee == null) {
                throw new EntityNotFoundException("Employee", id);
            }
            return employee;
        }
    }

    @Override
    public Employee getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<Employee> query = session.createQuery("from Employee as e " +
                    "where e.user.email = :email", Employee.class);
            query.setParameter("email",email);
            List<Employee> employees = query.list();

            if (employees.isEmpty()) {
                throw new EntityNotFoundException("Employee", "email", email);
            }

            return query.list().get(0);
        }
    }


    @Override
    public Employee getByUsername(String username) {

        try (Session session = sessionFactory.openSession()) {
            Query<Employee> query = session.createQuery("from Employee as e " +
                    "where e.user.username = :username", Employee.class);
            query.setParameter("username",username);
            List<Employee> employees = query.list();

            if (employees.isEmpty()) {
                throw new EntityNotFoundException("Employee", "username", username);
            }

            return query.list().get(0);
        }
    }

    @Override
    public Employee create(Employee employee) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.save(employee);
            tx.commit();
            return employee;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

    @Override
    public Employee update(Employee employee) {
        Transaction tx = null;

        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.update(employee);
            tx.commit();
            return employee;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

    @Override
    public Employee delete(Long id) {
        Transaction tx = null;
        Employee employee = getById(id);

        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.delete(employee);
            tx.commit();
            return employee;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

}
