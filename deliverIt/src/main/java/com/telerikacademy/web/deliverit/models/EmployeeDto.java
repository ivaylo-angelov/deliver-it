package com.telerikacademy.web.deliverit.models;

import javax.persistence.*;

public class EmployeeDto {

    private UserDto user;

    public EmployeeDto() {

    }

    public EmployeeDto(UserDto user) {
        setUser(user);
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}
