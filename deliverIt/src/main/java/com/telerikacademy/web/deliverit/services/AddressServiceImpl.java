package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.repositories.contracts.AddressRepository;
import com.telerikacademy.web.deliverit.services.contracts.AddressService;
import com.telerikacademy.web.deliverit.services.contracts.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    public static final String ADDRESS_CREATE_MODIFY_ERROR_MSG = "Address with street %s already exists in city %s.";
    private final AddressRepository repository;

    @Autowired
    public AddressServiceImpl(AddressRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Address> getAll() {
        return repository.getAll();
    }

    @Override
    public Address getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public boolean getByStreetAndCity(String street,City city) {
        return repository.getByCityAndStreet(street, city);
    }

    @Override
    public Address create(Address address, City city) {

        if (getByStreetAndCity(address.getStreet(), city)) {
            throw new DuplicateEntityException(
                    String.format(String.format(ADDRESS_CREATE_MODIFY_ERROR_MSG,
                            address.getStreet(), city.getName()),
                            address.getStreet(),
                            address.getCity().getName()));
        }

        return repository.create(address);
    }

    @Override
    public Address update(Address address, City city) {

        if (getByStreetAndCity(address.getStreet(), city)) {
            throw new DuplicateEntityException(
                    String.format(String.format(ADDRESS_CREATE_MODIFY_ERROR_MSG,
                            address.getStreet(), city.getName()),
                            address.getStreet(),
                            address.getCity().getName()));
        }

        return repository.update(address);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

}
