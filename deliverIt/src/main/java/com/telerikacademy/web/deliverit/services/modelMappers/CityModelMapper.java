package com.telerikacademy.web.deliverit.services.modelMappers;

import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.models.CityDto;
import com.telerikacademy.web.deliverit.models.Country;
import com.telerikacademy.web.deliverit.repositories.contracts.CityRepository;
import com.telerikacademy.web.deliverit.services.contracts.CityService;
import com.telerikacademy.web.deliverit.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CityModelMapper {

    private final CityService service;
    private final CountryService countryService;

    @Autowired
    public CityModelMapper(CityService service, CountryService countryService) {
        this.service = service;
        this.countryService = countryService;
    }

    public City fromDto(CityDto cityDto) {
        City city = new City();
        dtoToObject(cityDto, city);
        return city;
    }

    public City fromDto(CityDto cityDto, Long id) {
        City city = service.getById(id);
        dtoToObject(cityDto, city);
        return city;
    }

    private void dtoToObject(CityDto cityDto, City city) {
        Country country = countryService.getById(cityDto.getCountryId());
        city.setCountry(country);
        city.setName(cityDto.getName());
        city.setPostCode(cityDto.getPostCode());
    }
}
