package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.WarehouseSearchParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

public interface WarehouseService {

    List<Warehouse> getAll();

    List<Warehouse> filter(WarehouseSearchParameters wsp);

    Warehouse getById(Long id);

    Warehouse create(Warehouse warehouse);

    Warehouse update(Warehouse warehouse);

    void delete(Long id);

    Page<Warehouse> findPaginated(Pageable pageable);
}
