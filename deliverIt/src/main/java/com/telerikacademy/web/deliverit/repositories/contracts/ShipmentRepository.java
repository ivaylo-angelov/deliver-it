package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.ShipmentSearchParameters;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ShipmentRepository {

    List<Shipment> getAll();

    List<Shipment> filter(ShipmentSearchParameters ssp);

    Shipment getById(Long id);

    Shipment create(Shipment shipment);

    Shipment update(Shipment shipment);

    void delete(Shipment shipmentToDelete);

}

