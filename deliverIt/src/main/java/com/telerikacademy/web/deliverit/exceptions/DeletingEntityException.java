package com.telerikacademy.web.deliverit.exceptions;

public class DeletingEntityException extends RuntimeException{
    public DeletingEntityException(String message) {
        super(message);
    }
}
