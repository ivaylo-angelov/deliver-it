package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.AuthenticationFailureException;
import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.repositories.contracts.UserRepository;
import com.telerikacademy.web.deliverit.services.contracts.CityService;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import com.telerikacademy.web.deliverit.services.contracts.EmployeeService;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.services.modelMappers.CustomerModelMapper;
import com.telerikacademy.web.deliverit.services.modelMappers.EmployeeModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping
public class AuthenticationMvcController extends BasicAuthenticationMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final CustomerService customerService;
    private final CustomerModelMapper customerModelMapper;
    private final CityService cityService;
    private final UserRepository userRepository;
    private final ParcelService parcelService;
    private final EmployeeService employeeService;
    private final EmployeeModelMapper employeeModelMapper;

    @Autowired
    public AuthenticationMvcController(AuthenticationHelper authenticationHelper,
                                       CustomerService customerService,
                                       CustomerModelMapper customerModelMapper,
                                       CityService cityService,
                                       UserRepository userRepository,
                                       ParcelService parcelService, EmployeeService employeeService,
                                       EmployeeModelMapper employeeModelMapper) {
        super(authenticationHelper);
        this.authenticationHelper = authenticationHelper;
        this.customerService = customerService;
        this.customerModelMapper = customerModelMapper;
        this.cityService = cityService;
        this.userRepository = userRepository;
        this.parcelService = parcelService;
        this.employeeService = employeeService;
        this.employeeModelMapper = employeeModelMapper;
    }

    @ModelAttribute("cities")
    public List<City> populateCities() {
        return cityService.getAll();
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("loginDto", new LoginDto());
        model.addAttribute("registerDto", new RegisterDto());
        return "signIn-signUp";
    }

    @GetMapping("/profile")
    public String showProfilePage(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", user);
            Customer customer;
            if (!user.isEmployee()) {
                customer = customerService.getByUsername(user.getUsername());
                model.addAttribute("customer", customer);
            }

        } catch (UnauthorizedOperationException e) {
            return "unauthorized";
        }
        return "profile";
    }

    @GetMapping("/profile/edit")
    public String showEditProfilePage( Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        if (!user.isEmployee()) {
            Customer customer = customerService.getByUsername(user.getUsername());
            CustomerDto customerDto = customerModelMapper.toDto(customer);
            model.addAttribute("customer", customerDto);
            model.addAttribute("customerId",customer.getId());
            return "customerProfile-edit";
        } else {
            Employee employee = employeeService.getByUsername(user.getUsername());
            EmployeeDto employeeDto = employeeModelMapper.toDto(employee);
            model.addAttribute("employee", employeeDto);
            model.addAttribute("employeeId",employee.getId());
            return "employeeProfile-edit";
        }

    }

    @PostMapping("/employee/edit/{id}")
    public String editEmployeeProfile(
                              Model model,
                              @Valid @ModelAttribute("employee") EmployeeDto employee,
                              @PathVariable Long id,
                              BindingResult errors,
                              HttpSession session) {
        try {
            Employee employee1 = employeeModelMapper.fromDto(employee,id);
            employeeService.update(employee1);
            session.setAttribute("currentUserUsername",employee1.getUser().getUsername());
        }catch (DuplicateEntityException e) {
            if (e.getMessage().contains("username")) {
                errors.rejectValue("username", "duplicate_username", e.getMessage());
            } else {
                errors.rejectValue("email", "duplicate_email", e.getMessage());
            }
            return "employeeProfile-edit";
        }catch (EntityNotFoundException e){
            return "not-found";
        }

        return "redirect:/profile";
    }

    @PostMapping("/customer/edit/{id}")
    public String editCustomerProfile(Model model,
                              @Valid @ModelAttribute("customer") CustomerDto customer,
                              @PathVariable Long id,
                              BindingResult errors,
                              HttpSession session) {
        try {
            Customer customer1 = customerModelMapper.fromDto(customer,id);
            customerService.update(customer1);
            session.setAttribute("currentUserUsername",customer1.getUser().getUsername());
        }catch (DuplicateEntityException e) {
            if (e.getMessage().contains("username")) {
                errors.rejectValue("username", "duplicate_username", e.getMessage());
            } else {
                errors.rejectValue("email", "duplicate_email", e.getMessage());
            }
            return "customerProfile-edit";
        }catch (EntityNotFoundException e){
            return "not-found";
        }
        return "redirect:/profile";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("loginDto") LoginDto loginDto,
                              BindingResult errors,
                              HttpSession session,
                              Model model) {
        if (errors.hasErrors()) {
            return "signIn-signUp";
        }

        try {
            authenticationHelper.verifyAuthentication(loginDto.getUsername(), loginDto.getPassword());
            session.setAttribute("currentUserUsername", loginDto.getUsername());
        } catch (AuthenticationFailureException e) {
            errors.rejectValue("username", "auth_error", e.getMessage());
            model.addAttribute("registerDto", new RegisterDto());
            return "/signIn-signUp";
        }

        return "redirect:/";
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUserUsername");
        return "redirect:/";
    }

  /*  @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("registerDto", new RegisterDto());
        return "signIn-signUp";
    }*/

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("registerDto") RegisterDto registerDto,
                                 BindingResult bindingResult,
                                 Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("loginDto", new LoginDto());
            return "signIn-signUp";
        }

        if (!registerDto.getPassword().equals(registerDto.getRepeatPassword())) {
            bindingResult.rejectValue("password", "password_error", "Passwords don't match!");
        }

        try {
            Customer customer = customerModelMapper.fromRegisterDto(registerDto);
            customerService.create(customer);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
        }

        return "redirect:/login";
    }

}
