package com.telerikacademy.web.deliverit.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.telerikacademy.web.deliverit.models.enums.Status;
import com.telerikacademy.web.deliverit.models.Parcel;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "shipments")
public class Shipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shipment_id")
    private Long id;

    @Column(name = "departure_date")
    private LocalDate departureDate;

    @Column(name = "arrival_date")
    private LocalDate arrivalDate;

    @JsonIgnore
    @JsonInclude()
    @Transient
    private Status status;

    public Shipment() {

    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void calcStatus(List<Parcel> parcels) {

        if (departureDate.isAfter(LocalDate.now())
                && arrivalDate.isAfter(LocalDate.now())
                && parcels.size() == 0) {
            setStatus(Status.PREPARING);
        } else if (departureDate.isBefore(LocalDate.now())
                && arrivalDate.isAfter(LocalDate.now())
                && parcels.size() == 0) {
            setStatus(Status.PREPARING);
        } else if (departureDate.equals(LocalDate.now())
                && arrivalDate.isAfter(LocalDate.now())
                && parcels.size() == 0) {
            setStatus(Status.PREPARING);
        } else if (departureDate.isAfter(LocalDate.now())
                && arrivalDate.isAfter(LocalDate.now())
                && parcels.size() != 0) {
            setStatus(Status.PREPARING);
        } else if (departureDate.isBefore(LocalDate.now())
                && arrivalDate.isAfter(LocalDate.now())
                && parcels.size() != 0) {
            setStatus(Status.ONTHEWAY);
        } else if (departureDate.equals(LocalDate.now())
                && arrivalDate.isAfter(LocalDate.now())
                && parcels.size() != 0) {
            setStatus(Status.ONTHEWAY);
        } else if (departureDate.isBefore(LocalDate.now())
                && arrivalDate.isBefore(LocalDate.now())
                && parcels.size() != 0) {
            setStatus(Status.COMPLETED);
        } else if (departureDate.isBefore(LocalDate.now())
                && arrivalDate.isBefore(LocalDate.now())
                && parcels.size() == 0) {
            setStatus(Status.COMPLETED);
        } else if (departureDate.equals(LocalDate.now())
                && arrivalDate.equals(LocalDate.now())
                && parcels.size() == 0) {
            setStatus(Status.PREPARING);
        } else if (departureDate.isEqual(LocalDate.now())
                && arrivalDate.equals(LocalDate.now())
                && parcels.size() != 0) {
            setStatus(Status.COMPLETED);
        } else if (departureDate.isBefore(LocalDate.now()) && arrivalDate.isEqual(LocalDate.now())) {
            setStatus(Status.COMPLETED);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shipment shipment = (Shipment) o;
        return Objects.equals(id, shipment.id) && Objects.equals(departureDate, shipment.departureDate) && Objects.equals(arrivalDate, shipment.arrivalDate) && status == shipment.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, departureDate, arrivalDate, status);
    }
}
