package com.telerikacademy.web.deliverit.controllers.mvc;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.deliverit.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;

@Controller
public class BasicAuthenticationMvcController {

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public BasicAuthenticationMvcController(AuthenticationHelper authenticationHelper) {
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("currentUser")
    public User addCurrentUser(HttpSession session) {
        try {
            return authenticationHelper.tryGetUser(session);
        } catch (UnauthorizedOperationException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
