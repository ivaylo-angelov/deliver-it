package com.telerikacademy.web.deliverit.services.modelMappers;

import com.telerikacademy.web.deliverit.models.Country;
import com.telerikacademy.web.deliverit.models.CountryDto;
import com.telerikacademy.web.deliverit.repositories.contracts.CountryRepository;
import com.telerikacademy.web.deliverit.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.zip.CheckedOutputStream;

@Component
public class CountryModelMapper {

    private final CountryService service;

    @Autowired
    public CountryModelMapper(CountryService service) {
        this.service = service;
    }

    public Country fromDto(CountryDto countryDto) {
        Country country = new Country();
        dtoToObject(countryDto, country);
        return country;
    }

    public Country fromDto(CountryDto countryDto, Long id) {
        Country country = service.getById(id);
        dtoToObject(countryDto, country);
        return country;
    }

    private void dtoToObject(CountryDto countryDto, Country country) {
        country.setName(countryDto.getName());
    }
}
