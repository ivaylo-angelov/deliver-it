package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.WarehouseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.web.deliverit.repositories.QueryHelpers.like;

@Repository
@Transactional
public class WarehouseRepositoryImpl implements WarehouseRepository {

    private final SessionFactory sessionFactory;
    private final CustomerRepository customerRepository;

    @Autowired
    public WarehouseRepositoryImpl(SessionFactory sessionFactory, CustomerRepository customerRepository) {
        this.sessionFactory = sessionFactory;
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Warehouse> getAll() {
        try (Session session = sessionFactory.openSession()) {
            /*Query<Warehouse> query = session.createQuery("from Warehouse as w " +
                    "where w.address.city.name like concat('%', :city,'%') OR w.address.city.country.name like concat('%', :country,'%')", Warehouse.class);
            query.setParameter("city", city.orElse(""));
            query.setParameter("country", country.orElse(""));*/
            var query = session.createQuery("from Warehouse ", Warehouse.class);
            return query.list();
        }
    }

    @Override
    public List<Warehouse> filter(WarehouseSearchParameters wsp) {
        try (Session session = sessionFactory.openSession()) {
            var baseQuery = " from Warehouse ";
            var filters = new ArrayList<String>();

            if (!wsp.getStreet().isBlank()) {
                filters.add(" address.street like lower(:street) ");
            }

            if (wsp.getCityId() != null && wsp.getCityId() != -1) {
                filters.add(" address.city.id = :warehouseCityId ");
            }

            if (filters.size() > 0) {
                var finalQuery = String.join(" and ", filters);
                baseQuery = baseQuery + " where " + finalQuery;
            }


            Query<Warehouse> query = session.createQuery(baseQuery, Warehouse.class);

            if (!wsp.getStreet().isBlank()) {
                query.setParameter("street", like(wsp.getStreet()));
            }

            if (wsp.getCityId() != null && wsp.getCityId() != -1) {
                query.setParameter("warehouseCityId", wsp.getCityId());
            }

            return query.list();
        }

    }

    @Override
    public Warehouse getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            Warehouse warehouse = session.get(Warehouse.class, id);
            if (warehouse == null) {
                throw new EntityNotFoundException("Warehouse", id);
            }
            return warehouse;
        }
    }

    @Override
    public List<Warehouse> getByStreet(String street) {
        try (Session session = sessionFactory.openSession()) {
            Query<Warehouse> query = session.createQuery("from Warehouse as w " +
                    "where w.address.street = :street", Warehouse.class);
            query.setParameter("street", street);
            List<Warehouse> warehouses = query.list();

            if (warehouses.isEmpty()) {
                throw new EntityNotFoundException("Warehouse", "street", street);
            }

            return warehouses;
        }
    }

    @Override
    public Warehouse getByStreetAndCity(String street, City city) {
        try (Session session = sessionFactory.openSession()) {
            Query<Warehouse> query = session.createQuery("from Warehouse as w " +
                    "where w.address.street = :street and w.address.city.id = :city_id", Warehouse.class);
            query.setParameter("street", street);
            query.setParameter("city_id", city.getId());
            List<Warehouse> warehouses = query.list();

            if (warehouses.isEmpty()) {
                throw new EntityNotFoundException("Warehouse", "street", street);
            }

            return warehouses.get(0);
        }
    }

    @Override
    public Warehouse create(Warehouse warehouse) {
        Transaction tx = null;

        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.save(warehouse);
            tx.commit();
            return warehouse;

        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

    @Override
    public Warehouse update(Warehouse warehouse) {
        Transaction tx = null;

        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.update(warehouse);
            tx.commit();
            return warehouse;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }

    }

    @Override
    public Warehouse delete(Long id) {
        Transaction tx = null;
        Warehouse warehouse = getById(id);

        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.delete(warehouse);
            tx.commit();
            return warehouse;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

}
