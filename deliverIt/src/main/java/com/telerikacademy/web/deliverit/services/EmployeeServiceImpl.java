package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Employee;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.repositories.contracts.EmployeeRepository;
import com.telerikacademy.web.deliverit.services.contracts.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;


@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository repository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Employee> getAll(Optional<String> username,
                                 Optional<String> email,
                                 Optional<String> firstName,
                                 Optional<String> lastName) {
        return repository.getAll(username,email,firstName,lastName);
    }

    @Override
    public Employee getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Employee getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public Employee getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public Employee create(Employee employee) {
        //Optional<Employee> byEmail = repository.getByEmail(employee.getEmail());
        //Optional<Employee> byUsername = repository.getByUsername(employee.getUsername());

        boolean duplicateEmailExists = true;
        boolean duplicateUsernameExists = true;

        try {
            repository.getByEmail(employee.getUser().getEmail());
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("Employee", "email", employee.getUser().getEmail());
        }

        try {
            repository.getByUsername(employee.getUser().getUsername());
        } catch (EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }

        if (duplicateUsernameExists) {
            throw new DuplicateEntityException("Employee", "username", employee.getUser().getUsername());
        }

        return repository.create(employee);
    }

    @Override
    public Employee update(Employee employee) {
        boolean duplicateEmailExists = true;
        boolean duplicateUsernameExists = true;
        Employee existinEmployee;
        try {
            existinEmployee = repository.getByEmail(employee.getUser().getEmail());
            if(existinEmployee.getId().equals(employee.getId())){
                duplicateEmailExists = false;
            }
        } catch (EntityNotFoundException ex) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("Employee", "email", employee.getUser().getEmail());
        }

        try {
            existinEmployee = repository.getByUsername(employee.getUser().getUsername());
            if(existinEmployee.getId().equals(employee.getId())){
                duplicateUsernameExists = false;
            }
        } catch (EntityNotFoundException ex) {
            duplicateUsernameExists = false;
        }

        if (duplicateUsernameExists) {
            throw new DuplicateEntityException("Employee", "username", employee.getUser().getUsername());
        }

        return repository.update(employee);
    }

    @Override
    public Employee delete(Long id) {
        return repository.delete(id);
    }

    @Override
    public Page<Employee> findPaginated(Pageable pageable) {

        List<Employee> allEmployees = getAll(Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty());

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Employee> list;

        if (allEmployees.size()< startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allEmployees.size());
            list = allEmployees.subList(startItem, toIndex);
        }

        Page<Employee> employeePage
                = new PageImpl<Employee>(list, PageRequest.of(currentPage, pageSize), allEmployees.size());

        return employeePage;
    }

}

