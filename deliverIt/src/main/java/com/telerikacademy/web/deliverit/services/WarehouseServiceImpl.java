package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exceptions.DeletingEntityException;
import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Customer;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.WarehouseSearchParameters;
import com.telerikacademy.web.deliverit.repositories.contracts.ParcelRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.WarehouseRepository;
import com.telerikacademy.web.deliverit.services.contracts.CustomerService;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.web.deliverit.services.ShipmentServiceImpl.FIST_DELETE_PARCELS_ERROR_MSG;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    private final WarehouseRepository repository;
    private final CustomerService customerService;
    private final ParcelRepository parcelRepository;

    @Autowired
    public WarehouseServiceImpl(WarehouseRepository repository,
                                CustomerService customerService,
                                ParcelRepository parcelRepository) {
        this.repository = repository;
        this.customerService = customerService;
        this.parcelRepository = parcelRepository;
    }

    @Override
    public List<Warehouse> getAll() {
        return repository.getAll();
    }

    @Override
    public List<Warehouse> filter(WarehouseSearchParameters wsp) {
        return repository.filter(wsp);
    }

    @Override
    public Warehouse getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Warehouse create(Warehouse warehouse) {
        boolean duplicateExists = true;
        boolean duplicateCustomerAddress = true;

        try {
            Warehouse existingAddress = repository.getByStreetAndCity(warehouse.getAddress().getStreet(), warehouse.getAddress().getCity());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Address", "street", warehouse.getAddress().getStreet());
        }

        try {
            Customer customer = customerService.getByAddress(warehouse.getAddress());
        } catch (EntityNotFoundException e) {
            duplicateCustomerAddress = false;
        }

        if (duplicateCustomerAddress) {
            throw new DuplicateEntityException("Address", "street", warehouse.getAddress().getStreet());
        }


        return repository.create(warehouse);
    }

    @Override
    public Warehouse update(Warehouse warehouse) {
        boolean duplicateExists = true;
        boolean duplicateCustomerAddress = true;

        try {
            Warehouse existingAddress = repository.getByStreetAndCity(warehouse.getAddress().getStreet(),
                    warehouse.getAddress().getCity());
            if (warehouse.getAddress().getStreet().equals(existingAddress.getAddress().getStreet())) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Warehouse", "street", warehouse.getAddress().getStreet());
        }

        try {
            Customer customer = customerService.getByAddress(warehouse.getAddress());
        } catch (EntityNotFoundException e) {
            duplicateCustomerAddress = false;
        }

        if (duplicateCustomerAddress) {
            throw new DuplicateEntityException("Address", "street", warehouse.getAddress().getStreet());
        }

        return repository.update(warehouse);
    }

    @Override
    public void delete(Long id) {

        if (parcelRepository.isParcelExist(id)) {
            throw new DeletingEntityException(FIST_DELETE_PARCELS_ERROR_MSG);
        }

        repository.delete(id);
    }

    @Override
    public Page<Warehouse> findPaginated(Pageable pageable) {

        List<Warehouse> allWarehouses = getAll();

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Warehouse> list;

        if (allWarehouses.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allWarehouses.size());
            list = allWarehouses.subList(startItem, toIndex);
        }

        Page<Warehouse> warehousePage
                = new PageImpl<Warehouse>(list, PageRequest.of(currentPage, pageSize), allWarehouses.size());

        return warehousePage;
    }
}

