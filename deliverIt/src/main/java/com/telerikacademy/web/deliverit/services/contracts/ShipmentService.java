package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.ShipmentSearchParameters;
import com.telerikacademy.web.deliverit.models.Warehouse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ShipmentService {
    List<Shipment> getAll();

    List<Shipment> filter(ShipmentSearchParameters  ssp);

    Shipment getById(Long id);

    Shipment create(Shipment shipment);

    Shipment update(Shipment shipment);

    void getShipmentStatus(Shipment shipment);

    void delete(Long id);

   List<Parcel> getShipmentParcels(Long shipmentId);

    Page<Shipment> findPaginated(Pageable pageable);
}
