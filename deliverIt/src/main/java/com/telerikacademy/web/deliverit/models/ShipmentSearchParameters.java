package com.telerikacademy.web.deliverit.models;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class ShipmentSearchParameters {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate arrivalDate;


    public ShipmentSearchParameters(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
}
