package com.telerikacademy.web.deliverit.repositories.contracts;

import com.telerikacademy.web.deliverit.models.Address;
import com.telerikacademy.web.deliverit.models.City;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

public interface AddressRepository {

    List<Address> getAll();

    Address getById(Long id);

    boolean getByCityAndStreet(String street, City city);

    Address create(Address address);

    Address update(Address address);

    void delete(Long id);
}
