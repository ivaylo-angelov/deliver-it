package com.telerikacademy.web.deliverit.models;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class ShipmentSearchDto {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate arrivalDate;

    public ShipmentSearchDto() {
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
}
