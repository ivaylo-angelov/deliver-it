package com.telerikacademy.web.deliverit.repositories;

import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.repositories.contracts.AddressRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.CustomerRepository;
import com.telerikacademy.web.deliverit.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class CustomerRepositoryImpl implements CustomerRepository {
    private final SessionFactory sessionFactory;
    private final AddressRepository addressRepository;

    @Autowired
    public CustomerRepositoryImpl(SessionFactory sessionFactory, AddressRepository addressRepository) {
        this.sessionFactory = sessionFactory;
        this.addressRepository = addressRepository;;
    }

    @Override
    public List<Customer> getAll(Optional<String> firstName,
                                 Optional<String> lastName,
                                 Optional<String> email) {

        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("from Customer as c " +
                    "where c.user.firstName like concat('%', :firstName,'%') and c.user.lastName like concat('%', :lastName,'%') " +
                            "and c.user.email like concat('%', :email,'%')",
                    Customer.class);
            query.setParameter("firstName", firstName.orElse(""));
            query.setParameter("lastName", lastName.orElse(""));
            query.setParameter("email", email.orElse(""));
            return query.list();
        }

    }

   /* @Override
    public Integer getNumberOfAllCustomers(){
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("select count(*) from Customer as c "
                    ,Customer.class);
            System.out.println(query.list().get(0));
            return 1;
        }
    }*/

    @Override
    public Customer getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            Customer customer = session.get(Customer.class, id);
            if (customer == null) {
                throw new EntityNotFoundException("Customer", id);
            }
            return customer;
        }
    }

    @Override
    public Customer getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("from Customer as c " +
                    "where c.user.email = :email", Customer.class);
            query.setParameter("email",email);
            List<Customer> customer = query.list();

            if (customer.isEmpty()) {
                throw new EntityNotFoundException("Customer", "email", email);
            }

            return query.list().get(0);
        }
    }

    @Override
    public Customer getByAddress(Address address) {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("from Customer as c " +
                    "where c.address.city.id = :cityId and c.address.street = :street ", Customer.class);
            query.setParameter("street",address.getStreet());
            query.setParameter("cityId",address.getCity().getId());
            List<Customer> customer = query.list();

            if (customer.isEmpty()) {
                throw new EntityNotFoundException("Customer with address: street-%s and cityId-%d doesn't exist");
            }

            return query.list().get(0);
        }
    }


    @Override
    public Customer getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery("from Customer as c " +
                    "where c.user.username = :username", Customer.class);
            query.setParameter("username",username);
            List<Customer> employees = query.list();

            if (employees.isEmpty()) {
                throw new EntityNotFoundException("Employee", "username", username);
            }

            return query.list().get(0);
        }
    }

    @Override
    public Customer create(Customer customer) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.save(customer);
            tx.commit();
            return customer;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

    @Override
    public Customer update(Customer customer) {
        Transaction tx = null;

        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.update(customer);
            tx.commit();
            return customer;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
    }

    @Override
    public Customer delete(Long id) {
        Transaction tx = null;
        Customer customer = getById(id);
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.delete(customer);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
        return customer;
    }

}

