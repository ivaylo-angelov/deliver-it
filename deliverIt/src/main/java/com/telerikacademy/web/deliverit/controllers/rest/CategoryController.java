package com.telerikacademy.web.deliverit.controllers.rest;

import com.telerikacademy.web.deliverit.controllers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telerikacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Category;
import com.telerikacademy.web.deliverit.models.CategoryDto;
import com.telerikacademy.web.deliverit.services.contracts.CategoryService;
import com.telerikacademy.web.deliverit.services.modelMappers.CategoryModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService service;
    private final CategoryModelMapper modelMapper;
    private final AuthenticationHelper authorizationHelper;

    @Autowired
    public CategoryController(CategoryService service,
                              CategoryModelMapper modelMapper,
                              AuthenticationHelper authorizationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.authorizationHelper = authorizationHelper;
    }

    @GetMapping
    public List<Category> getAll(Optional<String> name)
    {
        return service.getAll(name);
    }

    @GetMapping("/{id}")
    public Category getById(@PathVariable @Valid Long id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Category create(@RequestHeader HttpHeaders headers, @Valid @RequestBody CategoryDto categoryDto) {
        try {
            Category category = modelMapper.fromDto(categoryDto);
            authorizationHelper.tryGetEmployee(headers);
            return service.create(category);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Category update(@RequestHeader HttpHeaders headers,
                           @PathVariable @Valid Long id,
                           @Valid @RequestBody CategoryDto categoryDto) {
        try {
            Category category = modelMapper.fromDto(categoryDto,id);
            authorizationHelper.tryGetEmployee(headers);
            return service.update(category);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @Valid @PathVariable Long id) {
        try {
            authorizationHelper.tryGetEmployee(headers);
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
